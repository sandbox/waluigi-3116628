#!/bin/bash
currDate=`date`
echo "-----------------${currDate}---------------------"
# Create folder for loaded data.
dataDir="/var/tmp/smh"

echo "Creating tmp folder"
mkdir -p $dataDir

echo "-------"
echo "Gather list of servers"
servers=$(/usr/local/bin/drush smh:listServer --root=/var/www/drupalvm/drupal/web)

echo "-------"
echo "Starting with data gathering"
printf "\n"

IFS=$";"
for server in $servers
do
  IFS=\, read -a fields <<<"$server"
  hostname=${fields[0]}
  root=${fields[1]}
  result=`ssh $hostname -o LogLevel=QUIET -oBatchMode=yes -o ConnectTimeout=3 -t "drush sm-gm --format=json --root=${root}"`

  echo "Starting with host ${hostname}"

  if [ -z "$result" ]
  then
    echo "Error on host ${hostname}. Connection failed."
  elif [[ $result != *"{"* ]]
  then
    echo "Error on host ${hostname}. Executing drush command failed. ${result}"
  else
    echo "Success"
    echo $result > "${dataDir}/${hostname}.json"
  fi
  printf "\n"
done

echo "-------"
echo "Start data migration to drupal"
# Migrate data.
/usr/local/bin/drush sims --root=/var/www/drupalvm/drupal/web
echo "-------"
echo "Removing tmp folder."
rm -r $dataDir

