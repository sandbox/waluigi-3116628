<?php

namespace Drupal\Tests\smh\Unit;

use Drupal\Component\Datetime\Time;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\smh\Entity\Server;
use Drupal\Tests\UnitTestCase;

/**
 * Class ServerUnitTests.
 *
 * @package Drupal\Tests\smh\Unit
 */
class ServerUnitTests extends UnitTestCase {

  /**
   * Tests the getDateDiffMinutes method from the server entity.
   */
  public function testGetDateDiffMinutes() {
    // Create time mock.
    $dateTime = $this->getMockBuilder(Time::class)->disableOriginalConstructor()
      ->getMock();

    // Overwrite getCurrentTime method.
    $dateTime->expects($this->any())
      ->method('getCurrentTime')
      ->will($this->returnValue(1583157204));

    // Create container mock.
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
    $container->set('datetime.time', $dateTime);

    // Create a server.
    $server = new Server(['lastRefresh' => 1583157144], 'server');

    $this->assertEquals(1, $server->getDateDiffMinutes());
  }

}
