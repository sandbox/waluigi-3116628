<?php

namespace Drupal\Tests\smh\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Class SMHTest.
 *
 * @package Drupal\Tests\smh\Functional
 */
class ServerBrowserTests extends BrowserTestBase {


  /**
   * The installed default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var array
   */
  protected static $modules = ['node', 'smh'];

  /**
   * A user that is allowed to create, delete, edit and list servers.
   *
   * @var \Drupal\user\Entity\User
   */
  private $user;

  /**
   * Perform initial setup tasks that run before every test method.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([
      'administer site configuration',
      'list server entities in backend',
      'add server entities',
      'delete server entities',
      'edit server entities',
    ]);

    // Create a default server.
    /** @var \Drupal\smh\Entity\Server $server */
    $server = $this->container->get('entity_type.manager')->getStorage('server')
      ->create([
        'id' => 'defaultserver',
        'label' => 'Default Server',
        'hostname' => 'hostname.com',
        'webDir' => '/path/to/webdir',
      ]);
    $server->save();
  }

  /**
   * Tests the permissions for the server list.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testServerListPermissions() {
    $user = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($user);
    $this->drupalGet('admin/config/system/server');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests the permissions for the server add form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testServerAddPermission() {
    $user = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($user);
    $this->drupalGet('admin/config/system/server/add');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests the permissions for the server delete form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testServerDeletePermission() {
    $user = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($user);
    $this->drupalGet('admin/config/system/server/defaultserver/delete');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests the permissions for the server edit form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testServerEditPermission() {
    $user = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($user);
    $this->drupalGet('admin/config/system/server/defaultserver/edit');
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Tests the "add" form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testAddServer() {
    // Login.
    $this->drupalLogin($this->user);

    // Access config page.
    $this->drupalGet('admin/config/system/server/add');

    $this->drupalPostForm(NULL, [
      'id' => 'testserver',
      'name' => 'Test Server',
      'hostname' => 'testserver.test',
      'webDir' => '/var/www/path',
    ], t('Save'));

    $this->assertSession()->pageTextContains('Created the Test Server Server.');
  }

  /**
   * Tests the server list in the backend.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testListServer() {
    // Login.
    $this->drupalLogin($this->user);

    // Get server table.
    $this->drupalGet('admin/config/system/server');

    // Ensure that values are visible.
    $this->assertSession()->pageTextContains('Default Server');
    $this->assertSession()->pageTextContains('hostname.com');
  }

  /**
   * Tests the "edit" form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testEditServer() {
    // Login.
    $this->drupalLogin($this->user);

    // Remove the server.
    $this->drupalGet('admin/config/system/server/defaultserver/edit');

    // Approve deletion.
    $this->drupalPostForm(NULL, [
      'name' => 'Edited Default Server',
    ], t('Save'));
    $this->assertSession()->pageTextContains('Saved the Edited Default Server Server.');
  }

  /**
   * Tests the "delete" form.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testDeleteServer() {
    // Login.
    $this->drupalLogin($this->user);

    // Remove the server.
    $this->drupalGet('admin/config/system/server/defaultserver/delete');

    // Approve deletion.
    $this->drupalPostForm(NULL, [], t('Delete'));
    $this->assertSession()->pageTextContains('Deleted Default Server.');
  }

  /**
   * Tests whether /servers is accessible as logged out user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testFrontendPermissionsLoggedOut() {
    // Access frontend page.
    $this->drupalGet('servers');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests whether /servers/{id} is accessible as logged out user.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testDetailView() {
    // Access frontend page.
    $this->drupalGet('servers/defaultserver');
    $this->assertSession()->statusCodeEquals(200);
  }

}
