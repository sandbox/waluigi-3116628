SMH - Simple Monitoring Hub
====
##About

This Module collects metrics from configured servers and displays it.
It is the counterpart of the [Simple Monitoring Module](https://www.drupal.org/sandbox/waluigi/3098637).
Also, it doesn't work without Simple Monitoring Module.

##Configuration

After you installed the module on a server, you have to configure a cron job that runs the bash
script in the module folder. This bash script will connect to the server via ssh, run the command from the Simple
Monitoring Module to collect the metrics, save it as JSON into the `/var/tmp/smh` folder, migrates the JSON to drupal and
removes the `/var/tmp/smh` folder again.

If your `tmp` folder is not under `/var/tmp`, be sure to change this the bash script so that the data can be migrated correctly.

Also, you have to create a log file in the `/var/log` folder and grant your user
access to it. This is needed because the cron job will be executed from your current user and will redirect the output
from the bash script into a log file in there for further debugging.

###Logging

####Create the File

First, lets create the log file. Therefore, run the following command:

`sudo touch /var/log/smh.log`

If you don't have the `/var/log` folder, feel free to store this file wherever you want. Just set up the cron job accordingly.

####Set the permissions

To check if your user can write to this file, you can either check the permissions manually using
`ls /var/log/smh.log -la` or, the easy way, try to write something to the file with `echo "hello" >> /var/log/smh.log`.
If you get the permission denied message, you have to grant your user access.

To do so, get your username with `whoami`.

Now, set your user as the owner of the log file:
`sudo chown vagrant /var/log/smh.log`

The log file is now ready for use.
###Cron Job
Now, lets set up the cron job. Run `crontab -e` to open the current users crontab file in your default editor.

We recommend the cron job to run every 10th minute, but you can define this on your own needs. If you need help setting
up the cron job, check out [crontab.guru](https://crontab.guru).

Also, if you have chosen a different path than `/var/log/smh.log`
for your log file, you have to adjust this here as well.
```
*/10 * * * * /path/to/your/drupal/web/modules/contrib/smh/loadData.sh >> /var/log/smh.log
```
This cronjob will run the bash script every 10th minute and redirect its outputs to our log file.
Save the crontab and you're ready to go. Now lets add a server in the drupal backend that has to be monitored.

##Add a new server

After you installed the module and set up the cron job and the log file, head over to your browser and navigate to
`admin/config/system/server` on your drupal site.

To add a new server, click "Add Server". You will then be asked for the name of the server, the hostname and the drupal
web directory.

**Server name**: This is the human readable name of your website.

**Hostname**: The hostname will be used to connect via SSH. It can either be the hostname as domain but also the IP.
Just ensure that you are able to connect over SSH **without** password but with public/private keys.

**Drupal Web directory**: This is the path to your /web directory on the server you're configuring.
It is used to run drush on the external system.

##

**Please make sure that you added the public key to the server that has to be monitored and that you can login via SSH without passwords.**

##
