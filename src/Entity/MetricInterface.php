<?php

namespace Drupal\smh\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Metric entities.
 *
 * @ingroup smh
 */
interface MetricInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Metric name.
   *
   * @return string
   *   Name of the Metric.
   */
  public function getName();

  /**
   * Sets the Metric name.
   *
   * @param string $name
   *   The Metric name.
   *
   * @return \Drupal\smh\Entity\MetricInterface
   *   The called Metric entity.
   */
  public function setName($name);

  /**
   * Gets the Metric creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Metric.
   */
  public function getCreatedTime();

  /**
   * Sets the Metric creation timestamp.
   *
   * @param int $timestamp
   *   The Metric creation timestamp.
   *
   * @return \Drupal\smh\Entity\MetricInterface
   *   The called Metric entity.
   */
  public function setCreatedTime($timestamp);

}
