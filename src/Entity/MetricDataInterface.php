<?php

namespace Drupal\smh\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Metric data entities.
 *
 * @ingroup smh
 */
interface MetricDataInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Metric data name.
   *
   * @return string
   *   Name of the Metric data.
   */
  public function getName();

  /**
   * Sets the Metric data name.
   *
   * @param string $name
   *   The Metric data name.
   *
   * @return \Drupal\smh\Entity\MetricDataInterface
   *   The called Metric data entity.
   */
  public function setName($name);

  /**
   * Gets the Metric data creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Metric data.
   */
  public function getCreatedTime();

  /**
   * Sets the Metric data creation timestamp.
   *
   * @param int $timestamp
   *   The Metric data creation timestamp.
   *
   * @return \Drupal\smh\Entity\MetricDataInterface
   *   The called Metric data entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the status code of the metric value.
   *
   * @return int
   *   The status code.
   */
  public function getStatusCode();

  /**
   * Set status code of the metric value.
   *
   * @param int $statusCode
   *   The new status code.
   *
   * @return \Drupal\smh\Entity\MetricDataInterface
   *   The called Metric data entity.
   */
  public function setStatusCode($statusCode);

  /**
   * Get metric value.
   *
   * @return \Drupal\smh\Entity\MetricDataInterface
   *   The called Metric data entity.
   */
  public function getMetricValue();

  /**
   * Set the metric value.
   *
   * @param string $metricValue
   *   The new metric value.
   *
   * @return \Drupal\smh\Entity\MetricDataInterface
   *   The called Metric data entity.
   */
  public function setMetricValue($metricValue);

  /**
   * Get the metric referenced to.
   *
   * @return \Drupal\smh\Entity\MetricDataInterface
   *   The called Metric data entity.
   */
  public function getMetricReference();

  /**
   * Set the metric referenced to.
   *
   * @param string $metricReference
   *   The new metric reference.
   *
   * @return \Drupal\smh\Entity\MetricDataInterface
   *   The called Metric data entity.
   */
  public function setMetricReference($metricReference);

}
