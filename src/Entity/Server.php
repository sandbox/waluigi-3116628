<?php

namespace Drupal\smh\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Server entity.
 *
 * @ConfigEntityType(
 *   id = "server",
 *   label = @Translation("Server"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\smh\ServerListBuilder",
 *     "form" = {
 *       "add" = "Drupal\smh\Form\ServerForm",
 *       "edit" = "Drupal\smh\Form\ServerForm",
 *       "delete" = "Drupal\smh\Form\ServerDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\smh\ServerHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "server",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/system/server/{server}",
 *     "add-form" = "/admin/config/system/server/add",
 *     "edit-form" = "/admin/config/system/server/{server}/edit",
 *     "delete-form" = "/admin/config/system/server/{server}/delete",
 *     "collection" = "/admin/config/system/server"
 *   }
 * )
 */
class Server extends ConfigEntityBase implements ServerInterface {

  /**
   * The Server ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Server label.
   *
   * @var string
   */
  protected $label;

  /**
   * Get the hostname of the server.
   *
   * @return string
   *   The hostname.
   */
  public function getHostname() {
    return $this->get('hostname');
  }

  /**
   * Get the unix timestamp of the last refresh.
   *
   * @return int
   *   The unix timestamp.
   */
  public function getLastRefresh() {
    return $this->get('lastRefresh');
  }

  /**
   * Get the webdir drush is located on the server.
   *
   * @return string
   *   The web directory.
   */
  public function getWebDir() {
    return $this->get('webDir');
  }

  /**
   * Get the over all status code of the server.
   *
   * @return int
   *   The status code.
   */
  public function getStatusCode() {
    return $this->get('statusCode');
  }

  /**
   * Get time difference between now and the last refresh in minutes.
   *
   * @return int
   *   The number of minutes.
   */
  public function getDateDiffMinutes() {
    $lastRefresh = $this->getLastRefresh();
    $currTime = \Drupal::time()->getCurrentTime();
    if (is_numeric($lastRefresh)) {
      $dateDiff = round(($currTime - $lastRefresh) / 60);
    }
    else {
      $dateDiff = -1;
    }
    return $dateDiff;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete() {
    parent::delete();
    $this->deleteAssociated();
  }

  /**
   * Deletes the associated metric entities of this server.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteAssociated() {
    try {
      $metrics = \Drupal::entityTypeManager()
        ->getStorage('metric')
        ->loadByProperties(['server' => $this->id()]);

      foreach ($metrics as $metric) {
        $metric->delete();
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('smh')->error($e);
    }
    catch (PluginNotFoundException $e) {
      \Drupal::logger('smh')->error($e);
    }
  }

}
