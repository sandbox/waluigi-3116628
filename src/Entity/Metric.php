<?php

namespace Drupal\smh\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Metric entity.
 *
 * @ingroup smh
 *
 * @ContentEntityType(
 *   id = "metric",
 *   label = @Translation("Metric"),
 *
 *   base_table = "metric",
 *   translatable = FALSE,
 *   admin_permission = "administer metric entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   field_ui_base_route = "metric.settings"
 * )
 */
class Metric extends ContentEntityBase implements MetricInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastRefresh() {
    return $this->get('lastRefresh')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastRefresh($timestamp) {
    $this->set('lastRefresh', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getServer() {
    return $this->get('server')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setServer($server) {
    $this->set('lastRefresh', $server);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusCode() {
    return $this->get('statusCode')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatusCode($statusCode) {
    $this->set('statusCode', $statusCode);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Metric entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['lastRefresh'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Last Refresh'))
      ->setDescription(t('Date and time of the last refresh.'))
      ->setSettings([
        'max_length' => 20,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['statusCode'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Status Code'))
      ->setDescription(t('The status code of the Metric'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['server'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Belongs to server'))
      ->setDescription(t('The name of the server. Start typing to search.'))
      ->setSetting('target_type', 'server')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Metric is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * Get time difference between now and the last refresh in minutes.
   *
   * @return int
   *   The number of minutes.
   */
  public function getDateDiffMinutes() {
    $lastRefresh = $this->getLastRefresh();
    $currTime = \Drupal::time()->getCurrentTime();
    if (is_numeric($lastRefresh)) {
      $dateDiff = round(($currTime - $lastRefresh) / 60);
    }
    else {
      $dateDiff = -1;
    }
    return $dateDiff;
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function delete() {
    parent::delete();

    // Remove associated metricData entities.
    $this->deleteAssociated();
  }

  /**
   * Deletes the associated metricData entities of this metric.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteAssociated() {
    try {
      $metrics = \Drupal::entityTypeManager()
        ->getStorage('metric_data')
        ->loadByProperties(['metricReference' => $this->id()]);

      foreach ($metrics as $metric) {
        $metric->delete();
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('smh')->error($e);
    }
    catch (PluginNotFoundException $e) {
      \Drupal::logger('smh')->error($e);
    }
  }

}
