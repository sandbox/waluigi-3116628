<?php

namespace Drupal\smh\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Server entities.
 */
interface ServerInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
