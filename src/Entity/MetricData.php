<?php

namespace Drupal\smh\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Metric data entity.
 *
 * @ingroup smh
 *
 * @ContentEntityType(
 *   id = "metric_data",
 *   label = @Translation("Metric data"),
 *
 *   base_table = "metric_data",
 *   translatable = FALSE,
 *   admin_permission = "administer metric data entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   field_ui_base_route = "metric_data.settings"
 * )
 */
class MetricData extends ContentEntityBase implements MetricDataInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusCode() {
    return $this->get('statusCode')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatusCode($statusCode) {
    $this->set('statusCode', $statusCode);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetricValue() {
    return $this->get('metricValue')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMetricValue($metricValue) {
    $this->set('metricValue', $metricValue);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetricReference() {
    return $this->get('metricReference')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMetricReference($metricReference) {
    $this->set('metricReference', $metricReference);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Metric data entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ]);

    $fields['statusCode'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Statuscode'))
      ->setDescription(t('The name of the Metric data entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ]);

    $fields['metricValue'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Value of the metric'))
      ->setDescription(t('The name of the Metric data entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ]);

    $fields['metricReference'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Belongs to metric'))
      ->setDescription(t('The ID of the metric. Start typing to search.'))
      ->setSetting('target_type', 'metric');

    $fields['status']->setDescription(t('A boolean indicating whether the Metric data is published.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));
    return $fields;
  }

}
