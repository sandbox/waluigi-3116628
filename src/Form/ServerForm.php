<?php

namespace Drupal\smh\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class ServerForm.
 */
class ServerForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    // Access control.
    $operationPermission = $this->getOperation() . ' server entities';
    if (!\Drupal::currentUser()->hasPermission($operationPermission)) {
      throw new AccessDeniedHttpException();
    }

    $form = parent::form($form, $form_state);

    $server = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Server name'),
      '#maxlength' => 255,
      '#default_value' => $server->label(),
      '#description' => $this->t("Name of the server"),
      '#required' => TRUE,
    ];

    $form['hostname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hostname'),
      '#maxlength' => 255,
      '#default_value' => $server->getHostname(),
      '#description' => $this->t("The Hostname one can connect over SSH to the server"),
      '#required' => TRUE,
    ];

    $form['webDir'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal Web directory'),
      '#maxlength' => 255,
      '#default_value' => $server->getWebDir(),
      '#description' => $this->t("The absolute path to the web directory on the server (e.g. /var/www/project/drupal/web)"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $server->id(),
      '#machine_name' => [
        'exists' => '\Drupal\smh\Entity\Server::load',
      ],
      '#disabled' => !$server->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $server = $this->entity;
    $status = $server->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Server.', [
          '%label' => $server->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Server.', [
          '%label' => $server->label(),
        ]));
    }
    $form_state->setRedirectUrl($server->toUrl('collection'));
  }

}
