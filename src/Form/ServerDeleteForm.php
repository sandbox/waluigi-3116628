<?php

namespace Drupal\smh\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Builds the form to delete Server entities.
 */
class ServerDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritDoc}
   */
  public function __construct() {
    // Avoid unauthorized access to deletion form.
    if (!\Drupal::currentUser()->hasPermission('delete server entities')) {
      throw new AccessDeniedHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.server.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Avoid unauthorized deletion of servers.
    if (!\Drupal::currentUser()->hasPermission('delete server entities')) {
      throw new AccessDeniedHttpException();
    }
    $this->entity->delete();

    $this->messenger()->addMessage(
      $this->t('Deleted @label.', [
        '@label' => $this->entity->label(),
      ])
    );

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
