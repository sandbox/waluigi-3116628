<?php

namespace Drupal\smh\Commands;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\smh\Entity\Metric;
use Drupal\smh\Entity\MetricData;
use Drupal\smh\Entity\Server;
use Drush\Commands\DrushCommands;
use JsonSchema\Uri\Retrievers\FileGetContents;

/**
 * A Drush commandfile.
 */
class SmhCommands extends DrushCommands {

  /**
   * Creates test data.
   *
   * @usage smh-commandName foo
   *   Usage description
   *
   * @command smh:createData
   * @aliases createData
   */
  public function createData() {
    for ($x = 0; $x < 100; $x++) {
      $server = Server::create([
        'id' => 'asdfads' . $x,
        'label' => 'test' . $x,
        'hostname' => '192.168.88.88',
        'webDir' => '/var/www/drupalvm/drupal/web',
      ]);
      $server->save();
    }
  }

  /**
   * Imports data from json files.
   *
   * @usage smh-commandName foo
   *   Usage description
   *
   * @command smh:importMetrics
   * @aliases sims
   */
  public function importData() {
    $baseDir = "/var/tmp/smh";
    $file = new FileGetContents();
    $servers = [];

    try {
      $servers = \Drupal::entityTypeManager()->getStorage('server')->loadMultiple();
    }
    catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('smh')->error($e);
    }
    catch (PluginNotFoundException $e) {
      \Drupal::logger('smh')->error($e);
    }

    foreach ($servers as $server) {
      // Clean up the existing metrics.
      $server->deleteAssociated();

      // Define the file name of the generated JSON file.
      $hostname = $server->getHostname();
      $filename = $baseDir . '/' . $hostname . '.json';

      if (file_exists($filename)) {
        $lastRefresh = 0;

        // 0 = success, 1 = warning, 2 = error
        $statusCode = 2;

        $content = $file->retrieve($filename);
        $content = Json::decode($content);

        foreach ($content as $metricId => $metricInfo) {

          // Set oldest date as last refresh.
          $lastRefresh = ($metricInfo['last_refresh'] < $lastRefresh || $lastRefresh === 0) ? $metricInfo['last_refresh'] : $lastRefresh;

          // Set lowest value available.
          $statusCode = ($metricInfo['statuscode'] < $statusCode) ? $metricInfo['statuscode'] : $statusCode;

          $metric = Metric::create([
            'name' => $metricInfo['title'],
            'lastRefresh' => $metricInfo['last_refresh'],
            'server' => $server->id(),
            'statusCode' => $metricInfo['statuscode'],
          ]
          );

          try {
            $metric->save();
          }
          catch (EntityStorageException $e) {
            \Drupal::logger('smh')->error($e);
          }

          foreach ($metricInfo['data'] as $metricData) {
            // Avoid value being longer than 50 chars.
            $value = (strlen($metricData['value']) > 50) ? substr($metricData['value'], 0, 50) : $metricData['value'];

            $dataEntity = MetricData::create([
              'name' => $metricData['title'],
              'statusCode' => $metricData['statuscode'],
              'metricValue' => $value,
              'metricReference' => $metric->id(),
            ]);

            try {
              $dataEntity->save();
            }
            catch (EntityStorageException $e) {
              \Drupal::logger('smh')->error($e);
            }
          }
        }
        try {
          $server->set('lastRefresh', $lastRefresh);
          $server->set('statusCode', $statusCode);
          $server->save();
        }
        catch (EntityStorageException $e) {
          \Drupal::logger('smh')->error($e);
        }
      }
      else {
        \Drupal::logger('smh')->error('File ' . $filename . ' for host ' . $hostname . ' not found. Skipping.');
      }
    }
  }

  /**
   * Lists servers.
   *
   * @usage smh-commandName foo
   *   Usage description
   *
   * @command smh:listServer
   * @aliases lss
   */
  public function listServer() {
    $serverNames = "";
    $servers = [];

    try {
      $servers = \Drupal::entityTypeManager()
        ->getStorage('server')
        ->loadMultiple();
    }
    catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('smh')->error($e);
    }
    catch (PluginNotFoundException $e) {
      \Drupal::logger('smh')->error($e);
    }

    foreach ($servers as $server) {
      $serverNames .= $server->getHostname() . "," . $server->getWebDir() . ";";
    }

    return $serverNames;
  }

}
