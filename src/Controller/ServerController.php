<?php

namespace Drupal\smh\Controller;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ServerController.
 */
class ServerController extends ControllerBase {

  /**
   * Serverlist.
   *
   * @return array
   *   Return Hello string.
   */
  public function serverList() {
    // Disable cache so the time is always up to date.
    \Drupal::service('page_cache_kill_switch')->trigger();
    $serverArr = [];

    try {
      $servers = \Drupal::entityTypeManager()->getStorage('server')->loadMultiple();

      foreach ($servers as $server) {
        $serverArr[] = [
          'name' => $server->label(),
          'link' => $server->id(),
          'hostname' => $server->getHostname(),
          'lastRefresh' => $server->getDateDiffMinutes(),
          'statusCode' => $server->getStatusCode(),
        ];
      }
    }
    catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('smh')->error($e);
    }
    catch (PluginNotFoundException $e) {
      \Drupal::logger('smh')->error($e);
    }

    return [
      '#theme' => 'server-list',
      '#servers' => $serverArr,
      '#title' => 'Servers',
    ];
  }

  /**
   * Preprocess the data array for the detail page.
   *
   * @param string $serverId
   *   The ID of the server.
   *
   * @return array
   *   Return server data array for the twig template.
   */
  public function serverDetails($serverId) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $data = [];
    $title = 'Server Details';
    try {
      $server = \Drupal::entityTypeManager()->getStorage('server')->load($serverId);

      if ($server === NULL) {
        throw new NotFoundHttpException();
      }

      $metrics = \Drupal::entityTypeManager()->getStorage('metric')->loadByProperties(['server' => [$serverId]]);
      $metricArr = [];
      $title = $server->label();

      foreach ($metrics as $metric) {
        $metrics = \Drupal::entityTypeManager()->getStorage('metric_data')->loadByProperties(['metricReference' => [$metric->id()]]);

        $dataArr = [];

        foreach ($metrics as $metricData) {
          $dataArr[] = [
            'title' => $metricData->label(),
            'statusCode' => $metricData->getStatusCode(),
            'metricValue' => $metricData->getMetricValue(),
          ];
        }
        $metricArr[] = [
          'title' => $metric->label(),
          'lastRefresh' => $metric->getDateDiffMinutes(),
          'statusCode' => $metric->getStatusCode(),
          'data' => $dataArr,
        ];
      }

      $data = [
        'name' => $server->label(),
        'hostname' => $server->getHostname(),
        'lastRefresh' => $server->getDateDiffMinutes(),
        'status' => $server->getStatusCode(),
        'metrics' => $metricArr,
      ];
    }
    catch (InvalidPluginDefinitionException $e) {
      \Drupal::logger('smh')->error($e);
    }
    catch (PluginNotFoundException $e) {
      \Drupal::logger('smh')->error($e);
    }

    return [
      '#theme' => 'server-detail',
      '#title' => $title,
      '#server' => $data,
    ];
  }

}
